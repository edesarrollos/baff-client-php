<?php

/**
 * MIT License
 *
 * Copyright (c) 2016 Gamaliel Espinoza Macedo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace edesarrollos\baff;

class Client {

    private $httpClient;

    const PROCESS_ENDPOINT = '/process';

    public function __construct($options=null) {
        $options = [];
        if (isset($options['host'])) {
            $options['base_uri'] = "http://{$options['host']}";
        } else {
            $options['base_uri'] = "http://127.0.0.1:4545";
        }
        $this->httpClient = new \GuzzleHttp\Client($options);
    }

    /**
     * Enqueue a process by requesting endpoint /enqueue
     *
     * @param $name
     * @return GuzzleHttp\Psr7\Response
     */
    public function enqueue($name) {
        $data = ['processName' => $name];
        $body = json_encode($data);
        
        $response = $this->request('POST', self::PROCESS_ENDPOINT);
        return $response;
    }

    /**
     * Make a request with the needed options for all
     * of the different requests.
     *
     * @param $httpMethod POST/GET
     * @param $url The endpoint without the base URL.
     * @return GuzzleHttp\Psr7\Response
     */
    private function request($httpMethod, $url) {
        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'appliation/json'
            ]
        ];
        $request = $this->httpClient->createRequest($httpMethod, $url, $options);
        return $this->httpClient->send($request);
    }

}